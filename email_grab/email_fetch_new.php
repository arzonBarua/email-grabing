<?php
include 'db.php';
/* connect to server */
$hostname = '{imap.gmail.com:993/imap/ssl/novalidate-cert}Label';
$username = 'username'; //bolwebclient@gmail.com
$password = 'password'; //client@dmin123

/* try to connect */
$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Tiriyo: ' . imap_last_error());

/* grab emails */
$emails = imap_search($inbox,'ALL');

/* if emails are returned, cycle through each... */
if(count($emails) > 0) {
	/* Fetch the last email only */
	//foreach($emails as $email){
		$last_mail = end($emails);
		$overview = imap_fetch_overview($inbox, $last_mail, 0);
		
		$last_mail = quoted_printable_decode(imap_fetchbody($inbox, $last_mail, 2));
		
		$emailDate = explode(" | ", $overview[0]->subject);
		$date = DateTime::createFromFormat('d/m/Y', trim($emailDate[1]));
		$emailDate = $date->format('Y-m-d');

		// echo "<pre>";
		// echo $emailDate;
		// print_r($last_mail);
		// echo "</pre>";


		// Check this email date has a entry in database or not?
		$rowsOnDate = mysqli_num_rows(mysqli_query($con, "SELECT * FROM igw_mail_his WHERE emailDate = '".$emailDate."'"));

		if($rowsOnDate){
			echo "Duplicate entry";
		} else {
			$document = new DOMDocument;
			@$document->loadHTML($last_mail);
			$xpath = new DOMXPath($document);
			$trs = $xpath->query("//tr");

			$finalValue = 0;
			$valueMatch = false;

			foreach ($trs as $key => $tr) {
				$td = $xpath->query('td', $tr);
				foreach ($td as $k => $value) {
					if($valueMatch){
						$finalValue =  (float) filter_var( $value->nodeValue, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
						break;
					}
					if($value->nodeValue == "MOS5") { $valueMatch = true; $finalName = $value->nodeValue; }
				}
				if($valueMatch) break;    
			}

			if($valueMatch){
				$number = mysqli_num_rows(mysqli_query($con, "SELECT `emailDate` FROM igw_mail_his WHERE `emailDate` = '".$emailDate."'"));
				if(!$number){
					$qry = mysqli_query($con, "INSERT INTO igw_mail_his (`name` , `value` , `emailDate`) VALUES ('".$finalName."', '".$finalValue."', '".$emailDate."')");
					if($qry)
						echo sprintf("Inserted: The final value is %s for %s.", $finalValue, $finalName);
					else
						echo "Not inserted.";
				} else {
					echo "Already exist...";
				}
				
			} else {
				$qry = mysqli_query($con, "INSERT INTO igw_mail_his (`name` , `value` , `emailDate`) VALUES ('MOS5', '0', '".$emailDate."')");
				if($qry)
					echo sprintf("Inserted: The final value is %s for %s.", $finalValue, $finalName);
				else
					echo "Not inserted.";
			}
		}
	//}
}

/* close the connection */
imap_close($inbox);
?>
